var db  = require('../models/db');

const migrationRepository = {

    async findByVersionAuthor(version, author)  {
        return new Promise(function(resolve, reject){
            db.migration.findOne({
                where: {
                    version: version,
                    author: author
                }
            }).then(function (result) {
                resolve(result);
            });
        })
    },

    async insert(migration) {
        new Promise(function(resolve, reject){
            db.migration.create({
                version: migration.version,
                author: migration.author,
                file_desc: migration.file_desc,
                file_name: migration.file_name,
                status: migration.status,
                create_date: Date.now()
            }).then(function (result) {
                resolve(result);
            }).catch(function (err) {
                reject(err);
            });
        });
    },

    async update(version, author, status) {
        return new Promise(function(resolve, reject){
            db.migration.update(
                {status: status},
                {where: {
                    version: version,
                    author: author
                }
            }).then(function (result) {
                resolve(result);
            });
        })
    }
};

module.exports = migrationRepository;
