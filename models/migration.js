module.exports = function(sequelize, DataTypes) {
    return sequelize.define('migration', {
        id: {
            type: DataTypes.INTEGER(10),
            allowNull: false,
            autoIncrement: true,
            unique: true,
            primaryKey: true
        },
        version: {
            type: DataTypes.STRING(20),
            allowNull: false,
        },
        author: {
            type: DataTypes.STRING(20),
            allowNull: false,
        },
        file_desc: {
            type: DataTypes.STRING(100),
            allowNull: false,
        },
        file_name: {
            type: DataTypes.STRING(200),
            allowNull: false,
        },
        status: {
            type: DataTypes.BOOLEAN,
            allowNull: false,
        },
        create_date: {
            type: DataTypes.DATE,
            allowNull: false,
            defaultValue: sequelize.literal('CURRENT_TIMESTAMP')
        }
    }, {
        tableName: 'migration'
    });
};
