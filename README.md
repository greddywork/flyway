# Fake Flyway

## Quick Start
Just support `mysql` database.

### Create Mysql
You can run a mysql service by docker.
```shell script
docker run --name mysql -e MYSQL_ROOT_PASSWORD=123456 -p 3306:3306 -d mysql
```

### Modify Config
Modify your config in `config.json`.
```json
{
  "db": {
    "url": "localhost",
    "username": "root",
    "password": "123456"
  }
}
```
### Create your sql script
You can create your sql script in `sql` folder.  
like:
- 2020070801_Greddy_sample_create_database.sql
```
CREATE DATABASE test DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
```
- 2020070802_Greddy_sample_sql_script.sql
```
CREATE DATABASE IF NOT EXISTS test DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;
USE test;

-- ----------------------------
-- Table structure for `accounts`
-- ----------------------------
DROP TABLE IF EXISTS `accounts`;
CREATE TABLE `accounts` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `account` varchar(190) CHARACTER SET utf8mb4 NOT NULL COMMENT '玩家账号',
  `mstatus` int(11) DEFAULT '0' COMMENT '玩家状态 0正常 1封号2黑名单3白名单',
  `memo` varchar(100) CHARACTER SET utf8mb4 DEFAULT NULL COMMENT '备注',
  `lastlogintime` timestamp NULL DEFAULT NULL COMMENT '最后登录时间',
  `createdate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updatedate` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
```