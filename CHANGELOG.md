# Fake Flyway changelog

The latest version of this file can be found at the master branch of the
flyway repository.

## 0.0.1 (2020-07-22)

### Initial

- Use like flyway and only support `mysql` database.