const execSQL   = require('./utils/exec.sql');
const path      = require('path');
const fs        = require('fs');
const config = require('./config');
const migrationRepository = require('./repository/migration.repository');

(async() => {
    execSQL.connect({
        'database': 'mysql',
        'host': config.db.url,
        'user': config.db.username,
        'password': config.db.password
    });
    await executeFileAsync(path.join(__dirname,'config/db_migrate_init.sql'));
    for (const filename of fs.readdirSync(path.join(__dirname, 'sql'))) {
        let nameArray = filename.split("_");
        let version = nameArray[0];
        let author = nameArray[1];
        let fileDesc =
            filename
                .replace(version+"_"+author+"_","")
                .replace(".sql", "")
                .replace(new RegExp('_', "gm"), " ");
        let migration = await migrationRepository.findByVersionAuthor(version, author);
        if(migration==null) {
            await migrationRepository.insert({
                version: version,
                author: author,
                file_desc: fileDesc,
                file_name: filename,
                status: 0
            })
            console.log(filename);
            await executeFileAsync(path.join(__dirname,'sql/'+filename))
                .then(function () {
                    migrationRepository.update(version, author, 1);
                });
        } else {
            if(migration.status==0) {
                await executeFileAsync(path.join(__dirname,'sql/'+filename))
                    .then(function () {
                        migrationRepository.update(version, author, 1);
                    });
            }
        }

    }
    execSQL.disconnect();
})();

async function executeFileAsync(filename) {
    return new Promise(function (resolve, reject) {
        execSQL.executeFile((filename), function(err) {
            if(err) {
                console.error(err);
                reject(err);
            } else {
                console.log(filename + " is finish!");
                resolve();
            }
        });
    });
}
