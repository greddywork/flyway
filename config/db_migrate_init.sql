CREATE DATABASE IF NOT EXISTS `db_migrate` DEFAULT CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci;

CREATE TABLE IF NOT EXISTS `db_migrate`.`migration` (
   `id` bigint(20) NOT NULL AUTO_INCREMENT,
   `version`     varchar(20)  CHARACTER SET utf8mb4 NOT NULL COMMENT '版本號',
   `author`      varchar(20)  CHARACTER SET utf8mb4 NOT NULL COMMENT '作者',
   `file_desc`   varchar(100) CHARACTER SET utf8mb4 NOT NULL COMMENT '檔名描述',
   `file_name`   varchar(200) CHARACTER SET utf8mb4 NOT NULL COMMENT '檔名',
   `status`      tinyint(1)   DEFAULT '0' COMMENT '狀態',
   `create_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '創建時間',
   PRIMARY KEY (`id`)
) ENGINE=MyISAM DEFAULT CHARSET=utf8;
